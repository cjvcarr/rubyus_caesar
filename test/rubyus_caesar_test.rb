require 'test_helper'

class RubyusCaesarTest < Minitest::Test

ALPHABET_LOWER = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z).freeze
  def test_encodes_with_single_character_keyword
		app = RubyusCaesar::Application.new
		assert_equal 'JMLps2VdPIRcr3YwtWLptWbkPm==', app.encode('Message for testing', [16])
  end

	def test_encodes_with_multi_character_keyword
		app = RubyusCaesar::Application.new
		assert_equal 'MANsv2JfSLFeu3BkvZOdvZeyRp==', app.encode('Message for testing', [19, 4, 18, 19])
	end
end
