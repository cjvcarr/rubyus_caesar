require 'rubyus_caesar/version'
require 'base64'
require 'optparse'

module RubyusCaesar
  ALPHABET_LOWER = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z).freeze

  class Application
    def parse
      options = {}
      parser = OptionParser.new do |opts|
        opts.banner = 'Usage: rubyus_caesar.rb [options]'
        opts.on('-m', '--message MESSAGE', 'Message to be encoded/decoded') do |message|
          options[:message] = message
        end
        opts.on('-k', '--key KEY', 'Key for encoding/decoding') do |key|
          options[:key] = key
        end
        opts.on('-e', '--encode', 'Encode the given message') do
          options[:encode] = true
        end
        opts.on('-d', '--decode', 'Decode the given message') do
          options[:decode] = true
        end
      end
      parser.parse!
      if options[:message].nil?
        print 'Enter Message: '
        options[:message] = gets.chomp
      end
      if options[:key].nil?
        print 'Enter Key: '
        options[:key] = gets.chomp
      end
      if options[:encode].nil? && options[:decode].nil?
        print 'Encode or Decode? '
        encode_decode = gets.chomp.downcase
        if encode_decode == 'encode'
          options[:encode] = true
        elsif encode_decode == 'decode'
          options[:decode] = true
        end
      end
      keynum = options[:key].split('')
      message = options[:message]
      for i in (0..keynum.size - 1)
        if ALPHABET_LOWER.include? keynum[i]
          keynum[i] = ALPHABET_LOWER.index(keynum[i])
        else
          next
        end
      end
      puts(encode(message, keynum)) if options[:encode]
			options[:decode] = false if options[:encode]
      puts(decode(message, keynum)) if options[:decode]
    end

    def encode(message, keynum)
      message_list = Base64.encode64(message).chomp.split('')
      #message_list = message.split('')
      encoded_message = []
      j = 0
      for i in (0..message_list.size - 1)
        message_list[i] = message_list[i].ord
        if (message_list[i] >= 97) && (message_list[i] <= 122)
          encoded_message << if (message_list[i] + keynum[j]) > 122
                               (message_list[i] - (26 - keynum[j])).chr
                             else
                               (message_list[i] + keynum[j]).chr
                             end
        elsif (message_list[i] >= 65) && (message_list[i] <= 90)
          encoded_message << if (message_list[i] + keynum[j]) > 90
                               (message_list[i] - (26 - keynum[j])).chr
                             else
                               (message_list[i] + keynum[j]).chr
                             end
        else
          encoded_message << (message_list[i]).chr
          next
        end
        if j == keynum.size - 1
          j = 0
        else
          j += 1
        end
      end
      encoded_message = encoded_message.join('')
    end

    def decode(message, keynum)
      message_list = message.split('')
      decoded_message = []
      j = 0
      for i in (0..message_list.size - 1)
        message_list[i] = message_list[i].ord
        if (message_list[i] >= 97) && (message_list[i] <= 122)
          decoded_message << if (message_list[i] - keynum[j]) < 97
                               (message_list[i] + (26 - keynum[j])).chr
                             else
                               (message_list[i] - keynum[j]).chr
                             end
        elsif (message_list[i] >= 65) && (message_list[i] <= 90)
          decoded_message << if (message_list[i] - keynum[j]) < 65
                               (message_list[i] + (26 - keynum[j])).chr
                             else
                               (message_list[i] - keynum[j]).chr
                             end
        else
          decoded_message << (message_list[i]).chr
          next
        end
        if j == keynum.size - 1
          j = 0
        else
          j += 1
        end
      end
			decoded_message = decoded_message.join('')
			decoded_message = Base64.decode64(decoded_message).force_encoding(Encoding::UTF_8)
    	end
    end
end
